# Lab2：ImageAnchor

## _ARGameViewController_

### 2-1 ARSession 初始化設定
```swift
override func viewDidLoad()
{
	super.viewDidLoad()

	let config = ARWorldTrackingConfiguration()
	config.detectionImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil)

	sceneView.session.run(config, options: .removeExistingAnchors)
	sceneView.delegate = self
}
```

### 2-2 實作 ARSCNViewDelegate
```swift
func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor)
{
	guard let _ = anchor as? ARImageAnchor else{return }
	
	// 取出 Max 模型
	guard let scene = SCNScene(named: "art.scnassets/max.scn")else{return }
	let max = scene.rootNode.childNode(withName: "Max_rootNode", recursively: true)!
	
	// 加到 node 子節點下
	node.addChildNode(max)
}
```

### 2-3 Build 實機，將鏡頭對準 kuma 圖

### [Lab2 完成專案下載](https://gitlab.com/jackychenkiki/arkit-workshop/-/archive/Lab2/arkit-workshop-Lab2.zip)