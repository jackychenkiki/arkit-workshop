# Lab3：動作

## _Max class_

### 3-1 初始化設定
```swift
init(scale: Float)
{
	super.init()
	
	// 3-2 載入模型
	setUpModel(scale)
	
	// 3-4 載入動作
	loadAnimations()
}
```

### 3-2 載入模型，並設定縮放比例
```swift
private func setUpModel(_ scale: Float)
{
	guard let scene = SCNScene(named: "art.scnassets/max.scn")else{fatalError("no Max scene") }

	guard let holderNote = scene.rootNode.childNode(withName: "Max_rootNode", recursively: true)else{fatalError("no Max_rootNode") }
	
	holderNote.scale = SCNVector3(SIMD3<Float>(repeating: scale))
	
	self.addChildNode(holderNote)
}
```

### 3-3 新增類別屬性：idleAnimation 和 walkAnimation
```swift
var idleAnimation: SCNAnimationPlayer!

var walkAnimation: SCNAnimationPlayer!
```

### 3-4 載入動作
```swift
private func loadAnimations()
{
	idleAnimation = loadAnimation(fromSceneNamed: "art.scnassets/max_idle.scn")
	walkAnimation = loadAnimation(fromSceneNamed: "art.scnassets/max_walk.scn")
	
	self.addAnimationPlayer(idleAnimation, forKey: "idle")
	self.addAnimationPlayer(walkAnimation, forKey: "walk")
	
	idleAnimation.play()
	walkAnimation.stop()
}
```

### 3-5 建立 walk 方法
```swift
private func walk()
{
	walkAnimation.play()
	idleAnimation.stop(withBlendOutDuration: 0.3)
}
```

### 3-6 建立 idle 方法
```swift
private func idle()
{
	idleAnimation.play()
	walkAnimation.stop(withBlendOutDuration: 0.3)
}
```

### 3-7 新增類別屬性：isWalking，並作 didSet 處理
```swift
var isWalking = false
{
	didSet
	{
		if oldValue != isWalking
		{
			isWalking ? walk() : idle()
		}
	}
}
```

---

## _ARGameViewController_

### 3-8 新增類別屬性：max
```swift
let max = Max(scale: 0.3)
```

### 3-9 修改 ARSCNViewDelegate 實作
```swift
func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor)
{
	guard let _ = anchor as? ARImageAnchor else{return }

	// 加到 node 子節點下
	node.addChildNode(max)
}
```


### 3-10 覆寫 touchesBegin 方法，實作 hitTest
```swift
override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
{
	guard touches.first?.view == sceneView else {return }
	
	guard let point = touches.first?.location(in: sceneView) else {return}
	
	guard let hitTestResult = sceneView.hitTest(point, options: nil).first else {return}
	
	guard hitTestResult.node.name == "Max" else {return }
	
	max.isWalking = !max.isWalking
}
```
### [Lab3 完成專案下載](https://gitlab.com/jackychenkiki/arkit-workshop/-/archive/Lab3/arkit-workshop-Lab3.zip)