//
//  ARGameViewController.swift
//  ARKitWorkshop
//
//  Created by JackyChen on 2019/9/5.
//  Copyright © 2019 labo. All rights reserved.
//

import UIKit
import ARKit

class ARGameViewController: UIViewController
{
    // MARK: properties
    
    // 3-8 新增類別屬性：max
    let max = Max(scale: 0.3)
    
    // 5-2 新增類別屬性：controlViewDirection
    var controlViewDirection = SIMD2<Float>(repeating: 0)
    
    // MARK: - IBOutlet
    
    @IBOutlet var sceneView: ARSCNView!
    
    @IBOutlet var controlView: UIView!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // 2-1 ARSession 初始化設定
        let config = ARWorldTrackingConfiguration()
        config.detectionImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil)
        
        sceneView.session.run(config, options: .removeExistingAnchors)
        sceneView.delegate = self
    }
}

// MARK: - touches

extension ARGameViewController
{
    // 3-10 覆寫 touchesBegin 方法，實作 hitTest
    // 5-1 touchesBegan 內容先註解掉（會被干擾）
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
//        guard touches.first?.view == sceneView else {return }
//
//        guard let point = touches.first?.location(in: sceneView) else {return}
//
//        guard let hitTestResult = sceneView.hitTest(point, options: nil).first else {return}
//
//        guard hitTestResult.node.name == "Max" else {return }
//
//        max.isWalking = !max.isWalking
    }
    
    // 5-3 覆寫 touchesMoved 方法，計算搖桿的向量參數
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        guard touches.first?.view == controlView else{return }
        
        guard let point = touches.first?.location(in: controlView) else {return}
        
        let center = ( x: controlView.bounds.width / 2, y: controlView.bounds.height / 2 )
        
        let x = point.x - center.x
        let y = point.y - center.y
        
        let direction = SIMD2<Float>( Float(x), Float(y) )
        
        controlViewDirection = normalize(direction)
    }
    
    // 5-4 覆寫 touchesEnded 方法，將向量歸零
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        guard touches.first?.view == controlView else{return }
        
        controlViewDirection = SIMD2<Float>(repeating: 0)
    }
}

// MARK: - ARSCNViewDelegate

extension ARGameViewController: ARSCNViewDelegate
{
    // 2-2 實作 ARSCNViewDelegate
    // 3-9 修改 ARSCNViewDelegate 實作
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor)
    {
        guard let _ = anchor as? ARImageAnchor else{return }
        
        node.addChildNode(max)
    }
}

// MARK: - SCNSceneRendererDelegate

extension ARGameViewController: SCNSceneRendererDelegate
{
    // 5-5 實作 SCNSceneRendererDelegate，將向量參數傳遞給 max
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval)
    {
        max.walkInDirection(direction: maxDirection, time: time)
    }
}

// MARK: - Computed properties

extension ARGameViewController
{
    // 5-6 新增計算屬性 maxDirection - 將搖桿向量轉換為以相機為準的向量
    var maxDirection: SIMD3<Float>
    {
        var direction = SIMD3<Float>(controlViewDirection.x, 0, controlViewDirection.y)

        if let pointOfView = sceneView.pointOfView
        {
            let p0 = pointOfView.presentation.convertPosition(SCNVector3Zero, to: nil)
            let p1 = pointOfView.presentation.convertPosition(SCNVector3(direction), to: nil)

            direction = SIMD3<Float>(p1.x - p0.x, 0.0, p1.z - p0.z)

            if direction != SIMD3<Float>(repeating: 0.0)
            {
                direction = normalize(direction)
            }
        }

        return direction
    }
}
