# 擴增實境人物控制實作
如果你對 AR 充滿好奇，不妨來體驗開發 AR App 的樂趣，ARKit 真假難辨的視覺效果，肯定不會讓你失望，經過這堂課後，您將擁有開發AR的基本能力。如：
 imageAnchor 設定、座標設定、轉角設定、比例縮放、材質設定、人物動作處理等。

### 環境需求
* 請攜帶內建 A9 以上晶片的設備，如 iphone6S 以上機種，平板體驗更佳。
* 設備OS版本需iOS12以上。
* 開發環境請安裝 Xcode 11。

### Agenda
* [Lab 1 AR 專案初始設定](/Lab/Lab1.md)
* [Lab 2 ImageAnchor](/Lab/Lab2.md)
* [Lab 3 動作](/Lab/Lab3.md)
* [Lab 4 陰影](/Lab/Lab4.md)
* [Lab 5 搖桿控制](/Lab/Lab5.md)

### 教材下載
* #### [Starter-project](https://gitlab.com/jackychenkiki/arkit-workshop/-/archive/starter/arkit-workshop-starter.zip)

* #### [Completed-project](https://gitlab.com/jackychenkiki/arkit-workshop/-/archive/master/arkit-workshop-master.zip)

